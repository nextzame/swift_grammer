# Swift_Grammer
### Swift Grammer summary
- Index<br>
[Flow Control](#flow)<br>
[Function](#function)<br>
[Array](#array)<br>
[Dictionary](#dictionary)<br>
[Set](#set)<br>
[Closure](#closure)<br>
[Enum](#enum)<br>
[Structure](#structure)<br>
[Class](#class)<br>
## flow
switch
```swift
var coordinate = (x: 10, y: 20)
//var coordinate: (x: Int, y: Int) = (10, 20)

switch coordinate {
case (10, 20):
    print("10,20")
case (let x, 20) where x==10:
    print("x=\(x)")
case (10, let y) where y==20:
    print("y=\(y)")
default:
    print("default")
}

```

## function
1. 함수 인자
```swift
//func 함수이름(외부함수인자이름 내부함수인자이름: 자료형)
func printTotalPrice(가격 price: Int, 갯수 count: Int) {
    print("Total Price: \(price * count)")
}
```
## array
순서가 있고 하나의 자료형만 저장 가능
1. 선언
```swift
var evenNumbers: [Int] = [2, 4, 6, 8]

var evenNumbers: [Int] = []
var evenNumbers = [Int]()
var evenNumbers: [Int] = [Int]()

var evenNumber2: Array<Int> = [2, 4, 6, 8]
var evenNumber2: Array<Int> = Array<Int>()
```
2. 추가
```swift
evenNumbers.append(10)
evenNumbers += [12, 14, 16]
evenNumbers.append(contentsOf: [18, 20])
```
3. 속성을 통한 접근
```swift
evenNumbers.count // 담긴 원소 갯수
evenNumbers.first // 첫번째 원소
evenNumbers.last // 마지막 원소
evenNumbers.isEmpty //원소가 하나도 없는지 확인
```
4. 함수
```swift
evenNumbers.min() //가장 작은 원소 찾기
evenNumbers.max() //가장 큰 원소 찾기

evenNumbers.contains(3) //3이 포함되었는지 확인
evenNumbers.insert(0, at: 0) //0번째에 0을 넣기
evenNumbers.remove(at: 0) //0번째 원소 삭제

evenNumbers.swapAt(0, 1) //0번째 원소와 1번째 원소를 교체

let firstThreeRemoved = evenNumbers.dropFirst(3) //실제 배열의 원소들을 지우지 않고 앞에 3개를 지우고 넘겨주기
let lastThreeRemoved = evenNumbers.dropLast(3) //실제 배열의 원소들을 지우지 않고 뒤의 3개를 지우고 넘겨주기
let firstThree = evenNumbers.prefix(3) //배열의 앞 3개 원소를 넘겨주기
let lastThree = evenNumbers.suffix(3) //배열의 뒤 3개 원소를 넘겨주기
```
5. subscript을 통한 접근
```swift
var firstItem = evenNumbers[0]
var firstThree = evenNumbers[0...2]

evenNumbers[0] = -2 //0번째 원소를 -2로 수정
evenNumbers[0...2] = [-2, 0 , 2] //0~2번째 원소를 -2, 0, 2로 수정
```
6. for문에서 사용하기
```swift
for num in evenNumbers {
    print(num)
}

//원소와 배열의 위치를 한번에 알아내기
for (index, num) in evenNumbers.enumerated() {
    print("idx: \(index), value: \(num)")
}
```
## dictionary
1. Dictionary 선언
```swift
var st: Dictionary<String, String> = ["name": "na", "job": "ios", "city":"istanbul"]
var st: Dictionary<String, String> = Dictionary<String, String>()
````
```swift
var st: [String: String] = [] 
var st: [String: String] = ["name": "na", "job": "ios", "city":"istanbul"]
var st = [String: String]
````
2. Dictionary에 원소 접근방법
````swift
st["city"] = "busan"
````
3. Dictionary 함수
````swift
st.isEmpty
st.count
````
4. Dictionary 수정, 추가, 삭제 
    1. 원소 업데이트
    ````swift
    st["name"] = "haha"
    ````
    2. 원소 추가
    ````swift
    st["age"] = "19"
    ````
    3. 원소 삭제
    ````swift
    st["age"] = nil
    ````
5. for문에서 사용하기
````swift
//Dictionary doesn't exist index
//Dictionary isn't listed in order
var scoreDic : [String: String] = ["nana": "23", "jon": "55", "hao": "152"]

for (name, score) in scoreDic {
    print("\(name), \(score)")
}

for key in scoreDic.keys {
    print(key)
}
````
6. Dictionary 사용 예제
````swift
var myDic: [String: String] = ["height": "193", "weight": "250", "age":"12"]

myDic["city"] = "mona"

func printHeightAndCity(dic: [String: String]) {
    if let height = dic["height"], let city = dic["city"] {
        print("height=\(height), city=\(city)")
    } else {
        print("요소가 없어요")
    }
}

printHeightAndCity(dic: myDic)
````
## set
1. Set 선언
````swift
//Set doesn't exist index
//Set isn't listed in order
//중복된 값이 저장되지 않음, 유일한 값만 저장됨
var soSet: Set<Int> = [1, 2, 3, 1, 2]
````
2. Set 함수
````swift
soSet.isEmpty
soSet.count
soSet.contains(4)
soSet.contains(1)
soSet.insert(5)
soSet.remove(1)
````
## closure
1. 기본 사용 모양, 변형
````swift
var Closure: (Int, Int) -> Int = { (a: Int, b: Int) -> Int in
    return a * b
}

let result = Closure(2, 2)
````
이 부분 변형<br>
{ (a: Int, b: Int) -> Int in<br>
    return a * b<br>
}

변형1 | 변형2 | 변형3 | 변형4
:--------: | :--------: | :--------: |:-----:
(a: Int, b: Int) -> Int  | (a, b) -> Int | (a, b) | a, b


변형1 | 변형2 | 변형3 
:---:|:-----:|:------:
return a * b | a * b | $0 * $1

변형1 | 변형2
:---:|:-----:
a, b in $0 * $1 | $0 * $1
- 축약1
````swift
var Closure: (Int, Int) -> Int = { $0 * $1 }
````
2. 선호 형태
````swift
var Closure: (Int, Int) -> Int = { a, b in
    return a * b
}
````
3. 사용 예
````swift
var Closure: (Int, Int) -> Int = { a, b in
    return a * b
}

func operate(a: Int, b: Int, operation: (Int, Int) -> Int) -> Int {
	let result = operation(a, b)
	return result
}
operate(a: 4, b: 2, operation: Closure)

var addClosure : (Int, Int) -> Int = { a, b in
	return a + b
}
operate(a: 4, b: 2, operation: addClosure)

//directly add closure in function
operate(a: 4, b: 2) { a, b in
	return a / b
}
````
## enum
1. 타입지정
```swift
//타입지정
//각각의 사례항목의 값을 지정하려면 타입을 지정하면 된다.

enum Fruits: String {
  case banana
  case apple
  case melon
  case tomato
}
```
2. switch문에서 사용
```swift
//enum의 항목의 값을 구분해서 쓸 때는 주로 switch case문을 사용한다.

let myFavorite: Fruits = .banana

switch myFavorite {
case .banana
  print(.banana)

case .apple
  print(.apple)

case .melon
  print(.melon)

case .tomato
  print(.tomato)
}

// Fruits.banana.rawValue == "banana"
```
3. 연관값
```swift
//연관값은 사례항목마다 각 타입값을 다르게 할 수 있다.
//바코드를 정의하는 방법은 2가지가 있을 수 있다. 1차원과 2차원.
//enum을 사용하면, 이렇게 2종류의 타입을 정의 할 수 있다.

enum Barcode {
    case upc(Int, Int, Int, Int)
    case qrCode(String)
}

var productBarcode = Barcode.upc(8, 85909, 51226, 3)

switch productBarcode {
case .upc(let numberSystem, let manufacturer, let product, let check):
    print("UPC: \(numberSystem), \(manufacturer), \(product), \(check).")

case .qrCode(let productCode):
    print("QR code: \(productCode).")
}
```
4. 값 설정
```swift
//Raw 값 (Raw Values)
//변하는 값이 아닌 하나의 값으로 채워질 수 있다.
//아래 코드는 아스키 코드값을 Raw Values로 저장하는 예이다.

enum ASCIIControlCharacter: Character {
    case tab = "\t"
    case lineFeed = "\n"
    case carriageReturn = "\r"
}
```
3. 사용예
```swift
enum Place: String {
  case
    park = "park",
    pool = "swimming pool",
    bars = "climbing bars",
    track1 = "running track",
    track2 = "walking track"
  static let facilities = [park, pool, bars, track1, track2]
  func enumFunction () -> Int {
    return -17
  }
  func enumChoiceFunction () -> String {
    switch self {
    case .track1, track2:
      return "running or walking"
    case .park:
      return "Walking, sitting on a bench, feeding birds"
    default:
      return "enjoying nature"
    }
  }
}
let i = Place.bars.enumFunction()
let e = Place.pool.rawValue
println (Place.pool)
let x = Place.track1.enumChoiceFunction()
var result: String = "
for amenity in Place.facilities {
  result = result + amenity.rawValue + ", "
}
println (result)
```
## structure
1.  튜플(tuple) 사용하기
````swift
let store1 = (x: 1, y: 7, name: "gs")
let store2 = (x: 4, y: 6, name: "seven")
let store3 = (x: 3, y: 5, name: "cu")

//거리 구하는 함수
func distance(current: (x: Int, y: Int), target: (x: Int, y: Int))->Double{
    let distanceX = Double(target.x - current.x)
    let distanceY = Double(target.y - current.y)
    let distance = sqrt(distanceX*distanceX+distanceY*distanceY)
    return distance
}

func printClosetStore(currentLocation:(x: Int, y: Int), stores:[(x: Int, y: Int, name: String)]){
    var closestStoreName = ""
    var closestStoreDistance = Double.infinity

    print("closestStoreDistance=\(closestStoreDistance)")

    for store in stores {
        let distanceToStore = distance(current: currentLocation, target: (x: store.x, y: store.y))
        closestStoreDistance = min(distanceToStore, closestStoreDistance)
        print("closestStoreDistance=\(closestStoreDistance), distanceToStore=\(distanceToStore)")
        if closestStoreDistance == distanceToStore {
            closestStoreName = store.name
            print("closestStoreName=\(closestStoreName)")
        }
    }
    print("Closest store: \(closestStoreName)")
}

printClosetStore(currentLocation: (3, 4), stores: [store1, store2, store3])
````
2. structure로 변환해서 사용하기
````swift
struct lecture {
    var lectureName: String
    var teacherName: String
    var TheNumOfStudents: Int
}

var lec1 = lecture(lectureName: "match", teacherName: "mina", TheNumOfStudents: 3)
var lec2 = lecture(lectureName: "sports", teacherName: "han", TheNumOfStudents: 4)
var lec3 = lecture(lectureName: "music", teacherName: "sofa", TheNumOfStudents: 5)

func GetLectureName(_ teacherName: String, _ lectures: [lecture]) -> String? {
//    for lec in lectures {
//        if teacherName == lec.teacherName {
//            lectureName = lec.lectureName
//            break
//        }
//    }
//    var lecture = lectures.first { lec in
//        return lec.lectureName == teacherName
//    }
    var lecture = lectures.first { $0.lectureName == teacherName }?.lectureName
    
    return lecture
}

var lectureName = GetLectureName("mina", [lec1, lec2, lec3])
print(lectureName)
````
3. structure 예제
````swift
struct lecture : CustomStringConvertible {
    var description: String {
        return "title:\(lectureName) instructor:\(teacherName)"
    }

    var lectureName: String
    var teacherName: String
    var TheNumOfStudents: Int
}

var lec1 = lecture(lectureName: "match", teacherName: "miha", TheNumOfStudents: 3)
var lec2 = lecture(lectureName: "sports", teacherName: "han", TheNumOfStudents: 4)
var lec3 = lecture(lectureName: "music", teacherName: "sofa", TheNumOfStudents: 5)

func GetLectureName(_ teacherName: String, _ lectures: [lecture]) -> lecture? {
//    for lec in lectures {
//        if teacherName == lec.teacherName {
//            lectureName = lec.lectureName
//            break
//        }
//    }
    
//    var lecture = lectures.first { lec in
//        return lec.lectureName == teacherName    
//    }

    var lecture = lectures.first { $0.lectureName == teacherName }

    return lecture
}

var lec = GetLectureName("mina", [lec1, lec2, lec3])
````
4. 프로토콜
````swift
struct Lecture: CustomStringConvertible {
	var description: String {
		return "Title: \(name), Instructor: \(instructor)"
	}

	let name: String
	let instructor: String
	let numOfStudent: Int
}

let lec1 = Lecture(name: "starter", instructor: "aha", numOfStudent: 5)

print(lec1)
````
5. 프로퍼티
````swift
struct Person {
	var firstName: String {
		willSet {
			print("willSet: \(firstName) --> \(newValue)")
		}
		
		didSet {
			print("didSet: \(oldValue) --> \(firstName)")
		}
	}
	var lastName: String

	lazy var isPopular: Bool = {
		if fullName == "son oh" {
			return true
		} else {
			return false
		}
	}()

	var fullName: String {
		get {
			return "\(firstName) \(lastName)"
		}

		set {
			if let firstName = newValue.components(separatedBy: " ").first {
				self.firstName = firstName
			}

			if let lastName = newValue.components(separatedBy: " ").last {
				self.lastName = lastName
			}
		}
	}

    mutating func lastNameChange(_ newName: String) {
        self.lastName = newName
    }

	static let isAlien: Bool = false
}

var person = Person(firstName: "hon", lastName: "mika")
person.firstName = "gogo"
person.lastNameChange("na")
person.isPopular
person.fullName
Person.isAlien
````
6. 메소드
````swift
struct Lecture {
    var title: String
    var maxStudents: Int = 20
    var numOfRegistered: Int = 0
    
    func remainSeats() -> Int {
        let remainSeats = maxStudents - numOfRegistered
        return remainSeats
    }
    
    mutating func register() {
        numOfRegistered += 1
    }
    
    static let target: String = "any"
    
    static func 소속학원이름() -> String {
        return "haha"
    }
}

var lec = Lecture(title: "begin")

lec.remainSeats()

lec.register()
lec.register()
lec.register()

lec.remainSeats()

Lecture.target
Lecture.소속학원이름()
````
7. 메소드 확장
```swift
struct Math {
    static func abs(value: Int) -> Int {
        if value > 0 {
            return value
        } else {
            return -value
        }
    }
    
}

extension Math {
    static func sqaure(value: Int) -> Int {
        return value * value
    }
    
    static func half(value: Int) -> Int {
        return value / 2
    }
}

Math.sqaure(value: 3)
Math.abs(value: -5)
Math.half(value: 10)


var value: Int = 10

extension Int {
    func square() -> Int {
        return self * self
    }
    
    func half() -> Int {
        return self/2
    }
}

value.square()
value.half()
```
## class
1. 클래스
```swift
struct PersonStruct {
	var firstNmae: String
	var lastName: String
	
	init(firstName: String, lastName: String) {
		self.firstName = firstName
		self.lastName = lastName
	}	

	var fullName: String {
		return "\(firstName) \(lastName)"
	}

	mutating func uppercaseName() {
		firstName = firstName.uppercased()
		lastName = lastName.uppercased()
	}
}

class PersonClass {
	var firstName: String
	var lastName: String

	init(firstNmae: String, lastName: String) {
		self.firstName = firstName
		self.lastName = lastName
	}

	var fullName: String {
		return "\(firstName) \(lastName)"
	}

	func uppercaseName() {
		firstName = firstName.uppercased()
		lastName = lastName.uppercased()
	}	
}

var personStruct1 = PersonStruct(firstName: "Jason", lastName: "Lee")
var personStruct2 = personStruct1

var personClass1 = PersonClass(firstName: "Jason", lastName: "Lee")
var personClass2 = personClass1

personStruct2.firstName = "Jay"
personStruct1.firstName
personStruct2.firstName

personClass2. firstName = "Jay"
personClass1.firstName
personClass2.firstName
```
2. struct, class 비교 사용
```swift
//struct
//1. 두 object를 '같다, 다르다'로 비교해야 하는 경우
//2. 복사된 각 개체들이 독립적인 상태를 가져야 하는 경우
//3. 코드에서 오브젝트의 데이터를 여러 스레드 걸쳐 사용할 경우
//- 각 스레드마다 서로 값이 다른 독립된 오브젝트 데이터를 접근함

//class
//1. 두 ojbect의 인스턴스 자체가 같음을 확인해야 할 때
//2. 하나의 객체가 필요하고, 여러 대상에 의해 접근되고 변경이 필요한 경우

//struct 사용 횟수 > enum 사용 횟수 > class 사용 횟수
```
3. 상속
```swift
struct Grade {
	var lettere: Character
	var points: Double
	var credits: Double
}

class Person {
	var firstName: String
	var lastName: String

	init(firstName: String, lastName: String) {
		self.firstName = firstName
		self.lastName = lastName
	}

	func printMyName() {
		print("My name is \(firstName) \(lastName)")
	}
}

class Student: Person {
	var grades: [Grade] = []
}

let jay = Person(firstName: "Jay", lastName: "Lee")
let jason = Student(firstName: "Jason", lastName: "Lee")

jay.firstName
jason.firstName

jay.printMyName()
jason.printMyName()

let math = Grade(letter: "B", points: 8.5, credits: 3)
let history = Grade(letter: "A", points: 10.0, credits: 3)

jason.grades.append(math)
jason.grades.appedn(history)

jason.grades.count
```
4. 상속 규칙
```swift
//상속 규칙
//1. 자식은 한 개의 superclass만 상속 받음
//2. 부모는 여러 자식들을 가질 수 있음
//3. 상속의 깊이는 상관이 없음

class StudentAthelete: Student {
	var minimumTrainingTime: Int = 2
	var trainedTime: Int = 0

	func train() {
		trainedTime += 1
	}
}

class FootballPlayer: StudentAthelete {
	var footballTeam = "Swift"

	override func train() {
		trainedTime += 2
	}
}

var athelete1 = StudentAthelete(firstName: "Yuna", lastName: "Jo")
var athelete2 = FootballPlayer(firstName: "Hesu", lastName: "Son")

athelete1.firstName
athelete2.firstName

athelete1.grades.append(math)
athelete2.grades.appedn(math)

athelete1.minimumTrainingTime
athelete2.minimumTrainingTime

athelte2.footballTeam

athelete1.train()
athelete1.trainedTime

athelete2.train()
athelete2.TrainedTime

athelete1 = athelete2 as StudentAthelete
athelete1.train()
athelete1.trainedTime

if let son = athelete1 as? FootballPlayer {
	print("--> team\(son.footballTeam)")
}
```
5. 상속을 사용할 때
```swift
//class 상속 규칙
//Single Responsibility (단일 책임)
//Type Safety (타입이 분명해야 할 때)
//Shared Base classes (다자녀가 있다)
//Extensibility (확장성이 필요한 경우)
//Identity (정체를 파악하기 위해)
```
6. 초기화(inialization) phase 2단계
```swift
class animal {
    var name: String
    var age : Int
    var weight : Int
    
    init(name: String, age: Int, weight: Int) {
        self.name = name
        self.age = age
        self.weight = weight
    }
}

class Bird: animal {
    var wing: Bool
    var leg: Bool
    
    init(_ wing: Bool, _ leg: Bool, _ name: String, _ age: Int, _ weight: Int) {
        self.wing = wing
        self.leg = leg //error leg 변수를 초기화하기 전에 접근함
        self.gogo //error leg 변수를 초기화하기 전에 접근함
        super.init(name: name, age: age, weight: weight)
        
    }
    
    func gogo() {
        print("nono")
    }
}

var eagle = Bird(false, false, "na", 13, 25)
```
7. convenience init
```swift
class animal {
    var name: String
    var age : Int
    var weight : Int
    
    init(name: String, age: Int, weight: Int) {
        self.name = name
        self.age = age
        self.weight = weight
    }
}

class Bird: animal {
    var wing: Bool
    var leg: Bool
    
    //designated init(initializer)
    override init(name: String, age: Int, weight: Int) {
        //phase 1
        self.wing = false
        self.leg = false
        //self.gogo()
        super.init(name: name, age: age, weight: weight)
        
        //phase 2 = phase 1(변수 초기화 과정)이 끝나야 변수나 함수 참조 가능
        self.gogo()
        
    }
    
    //convenience init(initializer)
    //인자가 너무 길 때 필요한 인자만 받아서 초기화
    //convenience init함수 안에서 designated init함수를 호출하게 됨
    convenience init(_ wing: Bool) {
        self.init(name: "nono", age: 13, weight: 14)
        
    }
    
    convenience init(ani: animal) {
        self.init(name: ani.name, age: ani.age, weight: ani.weight)
    }
    
    func gogo() {
        print("nono")
    }
}

var eagle = Bird(name: "na", age: 13, weight: 25)
var ani = animal(name: "ho", age: 35, weight: 22)
var so = Bird(ani: ani)
so.gogo()

//designated init은 자신의 부모의 designated init을 호출해야 함
//convenience init은 같은 클래스의 이니셜라이저를 꼭 하나 호출해야 함
//convenience init은 궁끅적으로는 designated init을 호출해야 함
```
